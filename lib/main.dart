import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:model_viewer/screen/home_controller.dart';
import 'package:model_viewer/screen/home_screen.dart';

void main() async {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomeSreen(),
      initialBinding: BindingsBuilder(() {
        Get.put(HomeController());
      }),
    );
  }
}
