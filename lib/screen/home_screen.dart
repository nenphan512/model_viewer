import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:model_viewer/screen/home_controller.dart';

class HomeSreen extends StatefulWidget {
  const HomeSreen({Key? key}) : super(key: key);

  @override
  _HomeSreenState createState() => _HomeSreenState();
}

class _HomeSreenState extends State<HomeSreen> {
  HomeController controller = HomeController();

  @override
  void initState() {
    super.initState();
    controller = Get.find<HomeController>();
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          bottom: PreferredSize(
            preferredSize: Size(0, 20),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.image,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    controller.pickFile();
                  },
                ),
                Expanded(
                  flex: 4,
                  child: TextFormField(
                    controller: controller.urlController,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      fillColor: Colors.black,
                      filled: true,
                      // enabledBorder: InputBorder.none,
                      // focusedBorder: InputBorder.none,
                      hintText: 'Enter url',
                      hintStyle: TextStyle(color: Colors.grey),
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(
                    Icons.download,
                    color: Colors.black,
                  ),
                  onPressed: () {
                    controller.downloadFile();
                  },
                ),
              ],
            ),
          ),
        ),
        body: Container(
          child: Center(
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 2,
                      child: controller.isLoading.value
                          ? Container(
                              child: Center(
                                child: CircularProgressIndicator(),
                              ),
                            )
                          : controller.cube.value,
                    ),
                  ],
                ),
                Positioned(
                    child: Container(
                  color: Colors.black45,
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          onPressed: () {
                            controller.zoomOut();
                          },
                          icon: Icon(
                            Icons.remove,
                            color: Colors.white,
                          ),
                          iconSize: 35,
                        ),
                        Text(
                          controller.zoom.toString(),
                          style: TextStyle(fontSize: 30, color: Colors.white),
                        ),
                        IconButton(
                          onPressed: () async {
                            controller.zoomIn();
                          },
                          icon: Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                          iconSize: 35,
                        ),
                      ]),
                ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
