import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:flutter_cube/flutter_cube.dart';
import 'package:path/path.dart';
import 'package:http/http.dart' as http;
import 'package:permission_handler/permission_handler.dart';

class HomeController extends GetxController {
  Object obj = Object();
  Rx<Widget> cube = Rx<Widget>(Container());
  TextEditingController urlController = TextEditingController();
  var zoom = 5.0.obs;
  String filePath = '';
  var isLoading = false.obs;

  @override
  void onInit() {
    obj = Object(fileName: 'assets/earth/earth_ball.obj');
    obj.lighting = true;
    setZoom();

    cube.value = Cube(
      onSceneCreated: (Scene scene) {
        scene.world.add(obj);
      },
    );
    super.onInit();
  }

  void zoomOut() {
    zoom.value -= 1;
    setZoom();
  }

  void zoomIn() {
    zoom.value += 1;
    setZoom();
  }

  void setZoom() {
    obj.scale.setValues(zoom.value, zoom.value, zoom.value);
    obj.updateTransform();
  }

  void setFile() {
    if (filePath != '') {
      var file = File(filePath);
      print('path:      ' + filePath);
      obj = Object(fileName: file.path, isAsset: false);
      obj.lighting = true;
      obj.updateTransform();

      setZoom();
      cube.value = Cube(
        onSceneCreated: (Scene scene) {
          scene.world.add(obj);
          // scene.camera.zoom = 6;
        },
      );

      isLoading.value = false;
    }
  }

  void pickFile() async {
    isLoading.value = true;
    FilePickerResult? result = await FilePicker.platform.pickFiles();

    String _path = result!.files.single.path!;
    filePath = _path;

    setFile();
  }

  void downloadFile() async {
    showSnackBar('Downloading file...', title: 'File');
    isLoading.value = true;
    var status = await Permission.storage.status;
    var url = urlController.text.trim();

    urlController.clear();

    final File _file = new File(url);
    final _filename = basename(_file.path).split('?')[0];

    //Check file extension
    if (_filename.split('.obj').length > 1) {
      var _filePath = '/storage/emulated/0/Download/' + _filename;

      //check permission
      if (status.isDenied) {
        await Permission.storage.request();
      }
      var response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        var file = File(_filePath);
        await file.writeAsBytes(response.bodyBytes);
        filePath = _filePath;

        showSnackBar('Download successful');
        setFile();
      } else {
        print('ERROR');
        showSnackBar('Download failed');
      }
    } else
      print('NOT OBJ FILE');
  }

  void showSnackBar(String mess, {String title = ''}) {
    Get.snackbar(title, mess, backgroundColor: Colors.white);
  }
}
